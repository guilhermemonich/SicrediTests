package br.com.sicredi.simulacao.tests.dto;

import java.math.BigDecimal;

public class SimulacaoDTO
{
    private Long id;
    private String nome;
    private String cpf;
    private String email;
    private BigDecimal valor;
    private Integer parcelas;
    private Boolean seguro;

    public SimulacaoDTO() {}

    public SimulacaoDTO(String nome, String cpf, String email, BigDecimal valor, Integer parcelas)
    {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
    }

    public SimulacaoDTO(String nome, String cpf, String email, BigDecimal valor, Integer parcelas, boolean seguro)
    {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
        this.seguro = seguro;
    }

    public SimulacaoDTO(Long id, String nome, String cpf, String email, BigDecimal valor, Integer parcelas, boolean seguro)
    {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
        this.seguro = seguro;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {return nome;}
    public void setNome(String nome) {this.nome = nome;}

    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getValor() {
        return valor;
    }
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getParcelas() {
        return parcelas;
    }
    public void setParcelas(Integer parcelas) {
        this.parcelas = parcelas;
    }

    public boolean isSeguro() {
        return seguro;
    }
    public void setSeguro(boolean seguro) {
        this.seguro = seguro;
    }

    @Override
    public String toString()
    {
        return String.format("Nome: %s - Cpf: %s - Email: %s - Valor: %s - Parcelas: %s - Seguro: %s", nome, cpf, email, valor, parcelas, seguro);
    }
}
