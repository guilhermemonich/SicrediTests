package br.com.sicredi.simulacao.tests;

import java.util.List;
import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import br.com.sicredi.simulacao.tests.dto.SimulacaoDTO;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;

public class SimulacoesTest
{
    private static final String url = "http://localhost:8080/api/v1/simulacoes/";

    private SimulacaoDTO createFirstSimulacao(){
        return new SimulacaoDTO(1L, "Guilherme", "05924124908", "guilherme.onoles@hotmail.com", new BigDecimal(2000), 6, false);
    }

    private SimulacaoDTO createSecondSimulacao(){
        return new SimulacaoDTO(2L,"Monich", "96325874125", "guilherme.monich@gmail.com", new BigDecimal(3000), 12, true);
    }

    public void cleanSimulacaoRepository()
    {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = given().when().get(url).as(JsonNode.class);
        List<SimulacaoDTO> simulacaoDTOList = mapper.convertValue(
                jsonNode,
                new TypeReference<List<SimulacaoDTO>>(){});

        for (final SimulacaoDTO item : simulacaoDTOList)
        {
            Long id = item.getId();
            given().when().delete(url.concat(id.toString()));
        }
    }

    @BeforeEach
    public void setUp()
    {
        cleanSimulacaoRepository();
    }

    @Test
    public void simulacoesGetAll_WithoutRecords_ExpectSizeZero()
    {
        int expectedSize = 0;
        int expectedStatusCode = 200;

        Response response = given().when().get(url);
        response.then().statusCode(expectedStatusCode);
        int obtainedSize = response.getBody().as(List.class).size();

        assertEquals(expectedSize, obtainedSize);
    }

    @Test
    public void simulacoesGetAll_WithOneRecord_ExpectSizeOne()
    {
        JsonNode node = new ObjectMapper().convertValue(createFirstSimulacao(), JsonNode.class);
        given().contentType("application/json").body(node).when().post(url);
        int expectedStatusCode = 200;
        int expectedSize = 1;

        Response response = given().when().get(url);
        response.then().statusCode(expectedStatusCode);
        int obtainedSize = response.getBody().as(List.class).size();

        assertEquals(expectedSize, obtainedSize);
    }

    @Test
    public void simulacoesGetAll_WithTwoExistingRecords_ExpectSizeTwoAndCorrectAttributes()
    {
        int expectedSize = 2;
        int expectedStatusCode = 200;
        SimulacaoDTO expectedSimulacaoDTOOne = createFirstSimulacao();
        SimulacaoDTO expectedSimulacaoDTOTwo = createSecondSimulacao();
        JsonNode nodeOne = new ObjectMapper().convertValue(expectedSimulacaoDTOOne, JsonNode.class);
        JsonNode nodeTwo = new ObjectMapper().convertValue(expectedSimulacaoDTOTwo, JsonNode.class);
        given().contentType("application/json").body(nodeOne).when().post(url);
        given().contentType("application/json").body(nodeTwo).when().post(url);

        Response response = given().when().get(url);
        response.then().statusCode(expectedStatusCode);
        JsonNode jsonNode = response.as(JsonNode.class);
        List<SimulacaoDTO> simulacaoDTOList = new ObjectMapper().convertValue(jsonNode, new TypeReference<List<SimulacaoDTO>>(){});
        int obtainedSize = response.getBody().as(List.class).size();

        assertEquals(expectedSize, obtainedSize);
        assertEquals(expectedSimulacaoDTOOne.getCpf(), simulacaoDTOList.get(0).getCpf());
        assertEquals(expectedSimulacaoDTOOne.getEmail(), simulacaoDTOList.get(0).getEmail());
        assertEquals(expectedSimulacaoDTOOne.getNome(), simulacaoDTOList.get(0).getNome());
        assertEquals(expectedSimulacaoDTOOne.getParcelas(), simulacaoDTOList.get(0).getParcelas());
        assertEquals(expectedSimulacaoDTOOne.getValor().stripTrailingZeros(), simulacaoDTOList.get(0).getValor().stripTrailingZeros());
        assertEquals(expectedSimulacaoDTOTwo.getCpf(), simulacaoDTOList.get(1).getCpf());
        assertEquals(expectedSimulacaoDTOTwo.getEmail(), simulacaoDTOList.get(1).getEmail());
        assertEquals(expectedSimulacaoDTOTwo.getNome(), simulacaoDTOList.get(1).getNome());
        assertEquals(expectedSimulacaoDTOTwo.getParcelas(), simulacaoDTOList.get(1).getParcelas());
        assertEquals(expectedSimulacaoDTOTwo.getValor().stripTrailingZeros(), simulacaoDTOList.get(1).getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesGet_InputCpfInexistent_ExpectStatus404()
    {
        String inputCpf = "12345678901";
        int expectedStatusCode = 404;

        Response response = given().when().get(url.concat(inputCpf));
        response.then().statusCode(expectedStatusCode);
    }

    @Test
    public void simulacoesGet_InputCpfExistent_ExpectStatus200AndCorrectReturnedAttributes()
    {
        String inputCpf = "05924124908";
        int expectedStatusCode = 200;
        SimulacaoDTO expectedSimulacaoDTO = createFirstSimulacao();
        JsonNode nodeOne = new ObjectMapper().convertValue(expectedSimulacaoDTO, JsonNode.class);
        JsonNode nodeTwo = new ObjectMapper().convertValue(createSecondSimulacao(), JsonNode.class);
        given().contentType("application/json").body(nodeOne).when().post(url);
        given().contentType("application/json").body(nodeTwo).when().post(url);

        Response response = given().when().get(url.concat(inputCpf));
        response.then().statusCode(expectedStatusCode);
        JsonNode jsonNode = response.as(JsonNode.class);
        SimulacaoDTO simulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);

        assertEquals(expectedSimulacaoDTO.getCpf(), simulacaoDTO.getCpf());
        assertEquals(expectedSimulacaoDTO.getEmail(), simulacaoDTO.getEmail());
        assertEquals(expectedSimulacaoDTO.getNome(), simulacaoDTO.getNome());
        assertEquals(expectedSimulacaoDTO.getParcelas(), simulacaoDTO.getParcelas());
        assertEquals(expectedSimulacaoDTO.getValor().stripTrailingZeros(), simulacaoDTO.getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesDelete_InputIdExistent_ExpectStatus204AndRemovedRecord()
    {
        int expectedSize = 1;
        int expectedStatusCode = 204;
        SimulacaoDTO removedSimulacaoDTO = createFirstSimulacao();
        SimulacaoDTO expectedSimulacaoDTO = createSecondSimulacao();
        JsonNode nodeOne = new ObjectMapper().convertValue(removedSimulacaoDTO, JsonNode.class);
        JsonNode nodeTwo = new ObjectMapper().convertValue(expectedSimulacaoDTO, JsonNode.class);
        given().contentType("application/json").body(nodeOne).when().post(url);
        given().contentType("application/json").body(nodeTwo).when().post(url);

        Response responseDelete = given().when().delete(url.concat(removedSimulacaoDTO.getId().toString()));
        responseDelete.then().statusCode(expectedStatusCode);

        Response response = given().when().get(url);
        List<SimulacaoDTO> simulacaoDTOList = new ObjectMapper().convertValue(response.as(JsonNode.class), new TypeReference<List<SimulacaoDTO>>(){});
        int obtainedSize = response.getBody().as(List.class).size();
        assertEquals(expectedSize, obtainedSize);
        assertEquals(expectedSimulacaoDTO.getCpf(), simulacaoDTOList.get(0).getCpf());
    }

    @Test
    public void simulacoesDelete_InputIdInexistentWithRecordsInDatabase_ExpectStatus404AndMessageBody()
    {
        int expectedStatusCode = 404;
        String expectedMessage = "Simulação não encontrada";
        SimulacaoDTO simulacaoDTOPost = createFirstSimulacao();
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoDTOPost, JsonNode.class)).when().post(url);
        String idInexistentToInput = Long.toString(simulacaoDTOPost.getId() + 1);

        Response responseDelete = given().when().delete(url.concat(idInexistentToInput));

        responseDelete.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedMessage));
    }

    @Test
    public void simulacoesDelete_InputIdInexistentWithEmptyRepository_ExpectStatus404AndMessageBody()
    {
        int expectedStatusCode = 404;
        String expectedMessage = "Simulação não encontrada";

        Response responseDelete = given().when().delete(url.concat("1"));

        responseDelete.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedMessage));
    }

    @Test
    public void simulacoesPost_InputCpfEmpty_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "CPF não pode ser vazio";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setCpf(null);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputCpfInvalidFormat_ExpectStatus400AndMessageBodyWithError()
    {
        String inputCpf = "059.241.249-08";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "CPF informado no formato inválido"; //Confirmar mensagem de erro com PO.
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setCpf(inputCpf);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputCpfInvalidFormatWithLessThan11Chars_ExpectStatus400AndMessageBodyWithError()
    {
        String inputCpf = "1234";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "CPF informado deve ter 11 caracteres"; //Não há critério de aceite, porém, acredito que o correto seria validar o CPF com 11 caracteres.
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setCpf(inputCpf);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputCpfInvalidFormatWithMoreThan11Chars_ExpectStatus400AndMessageBodyWithError()
    {
        String inputCpf = "123456789012";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "CPF informado deve ter 11 caracteres"; //Não há critério de aceite, porém, acredito que o correto seria validar o CPF com 11 caracteres.
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setCpf(inputCpf);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputCpfValid_ExpectStatus201()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getCpf(), obtainedSimulacaoDTO.getCpf());
    }

    @Test
    public void simulacoesPost_InputNomeEmpty_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Nome não pode ser vazio";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setNome(null);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputNomeValid_ExpectStatus201()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getNome(), obtainedSimulacaoDTO.getNome());
    }

    @Test
    public void simulacoesPost_InputEmailEmpty_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "E-mail não deve ser vazio"; //Saiu do padrão de mensagens, pedir para ajustar para "E-mail não pode ser vazio".
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setEmail(null);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputEmailInvalidWithoutAt_ExpectStatus400AndMessageBodyWithError()
    {
        String inputEmail = "guilherme.onoles";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "e-mail"; //As vezes a app emite "não é um endereço de e-mail", as vezes "E-mail deve ser um e-mail válido"
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setEmail(inputEmail);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputEmailInvalidWithoutUserName_ExpectStatus400AndMessageBodyWithError()
    {
        String inputEmail = "@hotmail.com";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "e-mail"; //As vezes a app emite "não é um endereço de e-mail", as vezes "E-mail deve ser um e-mail válido"
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setEmail(inputEmail);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputEmailInvalidWithoutServerName_ExpectStatus400AndMessageBodyWithError()
    {
        String inputEmail = "guilherme.onoles@";
        int expectedStatusCode = 400;
        String expectedErrorMessage = "e-mail"; //As vezes a app emite "não é um endereço de e-mail", as vezes "E-mail deve ser um e-mail válido"
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setEmail(inputEmail);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputEmailValid_ExpectStatus201()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getEmail(), obtainedSimulacaoDTO.getEmail());
    }

    @Test
    public void simulacoesPost_InputValorEmpty_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Valor não pode ser vazio";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(null);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputValorInvalidLessThan1000_ExpectStatus400AndMessageBodyWithError()
    {
        BigDecimal inputValor = new BigDecimal(999);
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Valor deve ser igual ou maior a R$ 1.000";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputValorValidEqualThan1000_ExpectStatus201()
    {
        BigDecimal inputValor = new BigDecimal(1000);
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getValor().stripTrailingZeros(), obtainedSimulacaoDTO.getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesPost_InputValorValidBiggerThan1000_ExpectStatus201()
    {
        BigDecimal inputValor = new BigDecimal(1001);
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getValor().stripTrailingZeros(), obtainedSimulacaoDTO.getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesPost_InputValorValidLessThan40000_ExpectStatus201()
    {
        BigDecimal inputValor = new BigDecimal(39999);
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getValor().stripTrailingZeros(), obtainedSimulacaoDTO.getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesPost_InputValorValidEqualThan40000_ExpectStatus201()
    {
        BigDecimal inputValor = new BigDecimal(40000);
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getValor().stripTrailingZeros(), obtainedSimulacaoDTO.getValor().stripTrailingZeros());
    }

    @Test
    public void simulacoesPost_InputValorInvalidBiggerThan40000_ExpectStatus400AndMessageBodyWithError()
    {
        BigDecimal inputValor = new BigDecimal(40001);
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Valor deve ser menor ou igual a R$ 40.000";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setValor(inputValor);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputParcelasEmpty_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Parcelas não pode ser vazio";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(null);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputParcelasInvalidLessThan2_ExpectStatus400AndMessageBodyWithError()
    {
        int inputParcelas = 1;
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Parcelas deve ser igual ou maior que 2";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputParcelasValidEqualThan2_ExpectStatus201()
    {
        int inputParcelas = 2;
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getParcelas(), obtainedSimulacaoDTO.getParcelas());
    }

    @Test
    public void simulacoesPost_InputParcelasValidBiggerThan2_ExpectStatus201()
    {
        int inputParcelas = 3;
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getParcelas(), obtainedSimulacaoDTO.getParcelas());
    }

    @Test
    public void simulacoesPost_InputParcelasValidLessThan48_ExpectStatus201()
    {
        int inputParcelas = 47;
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getParcelas(), obtainedSimulacaoDTO.getParcelas());
    }

    @Test
    public void simulacoesPost_InputParcelasValidEqualThan48_ExpectStatus201()
    {
        int inputParcelas = 48;
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertEquals(simulacao.getParcelas(), obtainedSimulacaoDTO.getParcelas());
    }

    @Test
    public void simulacoesPost_InputParcelasInvalidBiggerThan48_ExpectStatus400AndMessageBodyWithError()
    {
        int inputParcelas = 49;
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Parcelas deve ser menor ou igual a 48";
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setParcelas(inputParcelas);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    /* Teste comentado, o mapper da erro com campo booleano nulo
    @Test
    public void simulacoesPost_InputEmptySeguro_ExpectStatus400AndMessageBodyWithError()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessage = "Uma das opções de Seguro devem ser selecionadas";
        SimulacaoDTO simulacao = new SimulacaoDTO("Monich", "05924124908", "guilherme.onoles@hotmail.com", new BigDecimal(1000), 12);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }
    */
    @Test
    public void simulacoesPost_InputSeguroValidEqualThanFalse_ExpectStatus201()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setSeguro(false);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertFalse(obtainedSimulacaoDTO.isSeguro());
    }

    @Test
    public void simulacoesPost_InputSeguroValidEqualThanTrue_ExpectStatus201()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();
        simulacao.setSeguro(true);

        Response responsePost = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        responsePost.then().statusCode(expectedStatusCode);

        JsonNode jsonNode = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNode, SimulacaoDTO.class);
        assertTrue(obtainedSimulacaoDTO.isSeguro());
    }

    @Test
    public void simulacoesPost_InputCpfExistent_ExpectStatus409()
    {
        int expectedStatusCode = 409;
        String expectedErrorMessage = "CPF já existente";
        SimulacaoDTO simulacaoOne = createFirstSimulacao();
        SimulacaoDTO simulacaoTwo = createSecondSimulacao();
        simulacaoTwo.setCpf(simulacaoOne.getCpf());
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoOne, JsonNode.class)).when().post(url);

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoTwo, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPost_InputMultipleInvalidSituations_ExpectStatus400AndMessageBodyWithErrors()
    {
        int expectedStatusCode = 400;
        String expectedErrorMessageCPF = "CPF não pode ser vazio";
        String expectedErrorMessageNome = "Nome não pode ser vazio";
        String expectedErrorMessageParcelas = "Parcelas deve ser igual ou maior que 2";
        String expectedErrorMessageValor = "Valor deve ser menor ou igual a R$ 40.000";
        String expectedErrorMessageEmail = "E-mail deve ser um e-mail válido";

        SimulacaoDTO simulacao = new SimulacaoDTO();
        simulacao.setEmail("email");
        simulacao.setValor(new BigDecimal(41000));
        simulacao.setParcelas(1);
        simulacao.setSeguro(true);
        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);

        response.then().statusCode(expectedStatusCode).contentType("application/json").
                body(containsString(expectedErrorMessageCPF)).
                body(containsString(expectedErrorMessageNome)).
                body(containsString(expectedErrorMessageParcelas)).
                body(containsString(expectedErrorMessageValor)).
                body(containsString(expectedErrorMessageEmail));
    }

    @Test
    public void simulacoesPost_InputValidRecord_ExpectPropertiesReturnedInBodyMessage()
    {
        int expectedStatusCode = 201;
        SimulacaoDTO simulacao = createFirstSimulacao();

        Response response = given().contentType("application/json").body(new ObjectMapper().convertValue(simulacao, JsonNode.class)).when().post(url);
        JsonNode jsonNodeGet = given().when().get(url.concat(simulacao.getCpf())).as(JsonNode.class);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(jsonNodeGet.asText()));
    }

































    @ParameterizedTest
    @MethodSource("simulacoesPut_InvalidCpfRulesTestCase")
    void simulacoesPut_InputCpfFirstParameter_ExpectStatusSecondParameterAndMessageBodyErrorThirdParameter(String input, int expectedStatusCode, String expectedErrorMessage)
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);
        String bodyPut = String.format("{\"cpf\": \"%s\", \"seguro\": %s}", input, simulacaoRepository.isSeguro());

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    private static Stream<Arguments> simulacoesPut_InvalidCpfRulesTestCase() {
        return Stream.of(
                Arguments.of("059.241.249-08", 400, "CPF informado no formato inválido"),
                Arguments.of("1234", 400, "CPF informado deve ter 11 caracteres"),
                Arguments.of("123456789012", 400, "CPF informado deve ter 11 caracteres")
        );
    }

    @Test
    public void simulacoesPut_InputCpfValid_ExpectStatus200AndRecordUpdated()
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        int expectedStatusCode = 200;
        String input = "96325874125";
        String bodyPut = String.format("{\"cpf\": \"%s\", \"seguro\": %s}", input, simulacaoRepository.isSeguro());
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);
        //Recuperando o primeiro registro criado, para verificar o ID e comparar futuramente, verificando se realmente atualizou ou criou um novo
        JsonNode jsonNodeRepository = given().when().get(url.concat(simulacaoRepository.getCpf())).as(JsonNode.class);
        Long expectedId = new ObjectMapper().convertValue(jsonNodeRepository, SimulacaoDTO.class).getId();

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(input)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        assertEquals(expectedId, obtainedSimulacaoDTO.getId());
        assertEquals(input, obtainedSimulacaoDTO.getCpf());
        response.then().statusCode(expectedStatusCode);
        given().when().get(url.concat(simulacaoRepository.getCpf())).then().statusCode(404);
    }

    @Test
    public void simulacoesPut_InputNomeValid_ExpectStatus200AndRecordUpdated()
    {
        int expectedStatusCode = 200;
        String input = "Monich";
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        String bodyPut = String.format("{\"nome\": \"%s\", \"seguro\": %s}", input, simulacaoRepository.isSeguro());
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);

        String cpfToPut = simulacaoRepository.getCpf();
        Response responsePut = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(cpfToPut)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        assertEquals(input, obtainedSimulacaoDTO.getNome());
        responsePut.then().statusCode(expectedStatusCode);
    }

    @ParameterizedTest
    @MethodSource("simulacoesPut_EmailRulesTestCase")
    void simulacoesPut_InputEmailFirstParam_ExpectStatusSecondParamAndMessageBodyErrorThirdParamAndEmailFourthParam(String input, int expectedStatusCode, String expectedErrorMessage, String expectedEmail)
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);
        String bodyPut = String.format("{\"email\": \"%s\", \"seguro\": %s}", input, simulacaoRepository.isSeguro());

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(cpfToPut)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
        assertEquals(expectedEmail, obtainedSimulacaoDTO.getEmail());
    }

    private static Stream<Arguments> simulacoesPut_EmailRulesTestCase() {
        return Stream.of(
                Arguments.of("guilherme.onoles", 400, "e-mail", "guilherme.onoles@hotmail.com"),
                Arguments.of("@hotmail.com", 400, "e-mail", "guilherme.onoles@hotmail.com"),
                Arguments.of("guilherme.onoles@", 400, "e-mail", "guilherme.onoles@hotmail.com"),
                Arguments.of("guilherme.monich@gmail.com", 200, "", "guilherme.monich@gmail.com")

        );
    }

    @ParameterizedTest
    @MethodSource("simulacoesPut_ValorRulesTestCase")
    void simulacoesPut_InputValorFirstParam_ExpectStatusSecondParamAndMessageBodyErrorThirdParamAndValorFourthParam(BigDecimal input, int expectedStatusCode, String expectedErrorMessage, BigDecimal expectedValor)
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        String bodyPut = String.format("{\"valor\": %s, \"seguro\": %s}", input, simulacaoRepository.isSeguro());
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(cpfToPut)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));

        BigDecimal valorExpected = expectedValor.stripTrailingZeros();
        BigDecimal valorObtained = obtainedSimulacaoDTO.getValor().stripTrailingZeros();

        assertEquals(valorExpected, valorObtained);
    }

    private static Stream<Arguments> simulacoesPut_ValorRulesTestCase() {
        return Stream.of(
                Arguments.of(new BigDecimal(999), 400, "Valor deve ser igual ou maior a R$ 1.000", new BigDecimal(2000)),
                Arguments.of(new BigDecimal(1000), 200, "", new BigDecimal(1000)),
                Arguments.of(new BigDecimal(1001), 200, "", new BigDecimal(1001)),
                Arguments.of(new BigDecimal(39999), 200, "", new BigDecimal(39999)),
                Arguments.of(new BigDecimal(40000), 200, "", new BigDecimal(40000)),
                Arguments.of(new BigDecimal("40000.01"), 400, "Valor deve ser menor ou igual a R$ 40.000", new BigDecimal(2000))
        );
    }

    @ParameterizedTest
    @MethodSource("simulacoesPut_ParcelasRulesTestCase")
    void simulacoesPut_InputParcelaFirstParam_ExpectStatusSecondParamAndMessageBodyErrorThirdParamAndParcelaFourthParam(int input, int expectedStatusCode, String expectedErrorMessage, int expectedParcelas)
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        String bodyPut = String.format("{\"parcelas\": %s, \"seguro\": %s}", input, simulacaoRepository.isSeguro());
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(cpfToPut)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
        assertEquals(expectedParcelas, obtainedSimulacaoDTO.getParcelas());
    }

    private static Stream<Arguments> simulacoesPut_ParcelasRulesTestCase() {
        return Stream.of(
                Arguments.of(1, 400, "Parcelas deve ser igual ou maior que 2", 6),
                Arguments.of(2, 200, "", 2),
                Arguments.of(3, 200, "", 3),
                Arguments.of(47, 200, "", 47),
                Arguments.of(48, 200, "", 48),
                Arguments.of(49, 400, "Parcelas deve ser menor ou igual a 48", 6)
        );
    }

    @ParameterizedTest
    @MethodSource("simulacoesPut_SeguroRulesTestCase")
    void simulacoesPut_InputSeguroSecondParam_ExpectStatusThirdParamAndSeguroInputed(boolean repo, boolean input, int expectedStatusCode)
    {
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        simulacaoRepository.setSeguro(repo);
        String bodyPut = String.format("{\"seguro\": %s}", input);
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));
        JsonNode jsonNodeUpdated = given().when().get(url.concat(cpfToPut)).as(JsonNode.class);
        SimulacaoDTO obtainedSimulacaoDTO = new ObjectMapper().convertValue(jsonNodeUpdated, SimulacaoDTO.class);

        response.then().statusCode(expectedStatusCode);
        assertEquals(input, obtainedSimulacaoDTO.isSeguro());
    }

    private static Stream<Arguments> simulacoesPut_SeguroRulesTestCase() {
        return Stream.of(
                Arguments.of(false, true, 200),
                Arguments.of(true, false, 200)
        );
    }

    @Test
    public void simulacoesPut_InputCpfInexistent_ExpectStatus409()
    {
        int expectedStatusCode = 404;
        String cpfPut = "05924124908";
        String bodyPut = "{\"seguro\": false}";
        String expectedErrorMessage = String.format("CPF %s não encontrado", cpfPut);

        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfPut));

        response.then().statusCode(expectedStatusCode).contentType("application/json").body(containsString(expectedErrorMessage));
    }

    @Test
    public void simulacoesPut_InputMultipleInvalidSituations_ExpectStatus400AndMessageBodyWithErrors()
    {
        int expectedStatusCode = 400;
        SimulacaoDTO simulacaoRepository = createFirstSimulacao();
        String bodyPut = "{\"parcelas\": 1, \"email\": \"guilherme.onoles\", \"seguro\": true}";
        String expectedErrorMessageParcelas = "Parcelas deve ser igual ou maior que 2";
        String expectedErrorMessageEmail = "e-mail";
        given().contentType("application/json").body(new ObjectMapper().convertValue(simulacaoRepository, JsonNode.class)).when().post(url);

        String cpfToPut = simulacaoRepository.getCpf();
        Response response = given().contentType("application/json").body(bodyPut).when().put(url.concat(cpfToPut));

        response.then().statusCode(expectedStatusCode).contentType("application/json").
                body(containsString(expectedErrorMessageParcelas)).
                body(containsString(expectedErrorMessageEmail));
    }
}