package br.com.sicredi.simulacao.tests;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

public class RestricoesTest
{
    private static final String url = "http://localhost:8080/api/v1/restricoes/";

    @Test
    public void restricoes_InputNotRestrictedCpf_ExpectCode204AndEmptyMessageBody()
    {
        String inputCpf = "05924124908";
        String urlWithCpf = url.concat(inputCpf);
        String expectedMessage = "";
        int expectedStatusCode = 204;

        given().
        when().
            get(urlWithCpf).
        then().
            statusCode(expectedStatusCode).
            contentType(expectedMessage);
    }

    @ParameterizedTest
    @MethodSource("restricoes_RestrictedCpfTestCase")
    void restricoes_InputRestrictedCpfFirstParam_ExpectCode200AndRestrictionMessageBody(String inputCpf)
    {
        String urlWithCpf = url.concat(inputCpf);
        String expectedMessage = String.format("O CPF %s possui restrição", inputCpf);
        int expectedStatusCode = 200;

        given().
                when().
                get(urlWithCpf).
                then().
                statusCode(expectedStatusCode).
                contentType("application/json").body("mensagem", containsString(expectedMessage));
    }

    private static Stream<Arguments> restricoes_RestrictedCpfTestCase() {
        return Stream.of(
                Arguments.of("97093236014"),
                Arguments.of("60094146012"),
                Arguments.of("84809766080"),
                Arguments.of("62648716050"),
                Arguments.of("26276298085"),
                Arguments.of("01317496094"),
                Arguments.of("55856777050"),
                Arguments.of("19626829001"),
                Arguments.of("24094592008"),
                Arguments.of("58063164083")
        );
    }
}
